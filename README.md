# Manuscript Code Repository

**Manuscript title**: From Neglecting to Including Cultivar-Specific Per Se Temperature Responses: Extending the Concept of Thermal Time in Field Crops

Plant Phenomics 2024;6:Article 0185.
(https://doi.org/10.34133/plantphenomics.0185)



## Published Custom Software

Relevant custom software used for this manuscript that was published beforehand:

### Phenomics data processing I

Roth, L., Rodríguez-Álvarez, M. X., van Eeuwijk, F., Piepho, H.-P. & Hund, A. Phenomics data processing: A plot-level model for repeated measurements to extract the timing of key stages and quantities at defined time points. F. Crop. Res. 274, (2021).
(https://doi.org/10.1016/j.fcr.2021.108314).

 - (https://gitlab.ethz.ch/crop_phenotyping/htfp_data_processing#htfp-data-processing-i)

### Phenomics data processing II
Roth L, Piepho H-P, Hund A. 2022. Phenomics data processing: extracting dose–response curve parameters from high-resolution temperature courses
and repeated field-based wheat height measurements. In Silico Plants 2022: diac007; doi: (http://doi.org/10.1093/insilicoplants/diac007).

 - (https://gitlab.ethz.ch/crop_phenotyping/htfp_data_processing#htfp-data-processing-ii)

### LLT data processing

Nagelmüller, Sebastian, Norbert Kirchgessner, Steven Yates, Maya Hiltpold, and Achim Walter (2016). “Leaf Length Tracker: A Novel Approach to Analyse Leaf Elongation Close to the Thermal Limit of Growth in the Field”. In: Journal of Experimental Botany 67.6, pp. 1897–1906. (http://doi.org/10.1093/jxb/erw003).

 - (https://sourceforge.net/projects/leaf-length-tracker/)

### MARTRACK data processing

Mielewczik, Michael, Michael Friedli, Norbert Kirchgessner, and Achim Walter (2013). “Diel Leaf Growth of Soybean: A Novel Method to Analyze Two-Dimensional Leaf Expansion in High Tem-poral Resolution Based on a Marker Tracking Approach (Martrack Leaf)”. In: Plant Methods 9.30.
(https://doi.org/10.1186/1746-4811-9-30).
 - (https://sourceforge.net/projects/martrackleaf/)

## Specific processing code (this repository)

Software requirement R:
 - Operation system: Any
 - R version: 4.1.1
 - Special hardware requirements: None

Software requirements Python
 - Operation system: Any
 - Version: 3.9
 - Special hardware: CUDA GPU


### Soybean MARTRACK and FIP data processing

./R/Soy_Track/*.R

### Wheat LLT and FIP data processing

./R/Wheat_LLT/*.R

### Growth rate predictions 
./R/4_Estimate_FIP_parameters.R

./R/5_Predict_FIP_data.R

### Phenology predictions

./R/Wheat_LLT/6_Predict_Phenology_data.R

### Neural network training

./Python/TrainLLTRegressor.py


## Specific data (this repository)

./Data_*

## Instructions
R
 1. Set up a R 4.1.1 environment 
 2. Start with the file with the lowest number in each folder (e.g., 0_prepare_data.R). Each script writes its results to a *.RDA file, incremental processing is therefore possible.

Python
 1. Set up a conda Python 3.9 environment
 2. Run TrainLLTRegressor.py 
