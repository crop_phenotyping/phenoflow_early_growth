import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pytorch_lightning as pl
from torchmetrics import R2Score
from pytorch_lightning.strategies import DDPStrategy
import seaborn as sns
import torch
from lightning_lite.utilities.seed import seed_everything
from torch.optim import SGD, Adam
from torch.utils.data import TensorDataset, DataLoader
from torch.utils.data import random_split
import torch.nn.functional as F
from torch.optim.lr_scheduler import ExponentialLR
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.callbacks import EarlyStopping
from pytorch_lightning import loggers as pl_loggers


from multiprocessing import freeze_support

import pandas as pd
import numpy as np

from pathlib import Path

seed_everything(seed=1)


class LLTDataModule(pl.LightningDataModule):
    def __init__(self, data_path="Data_NN_train/", prefix="LLT_wheat", multi=True, covar_level="ref",
                 covariate="temp_air_10cm", num_workers=4, n_genotypes=12):
        super().__init__()
        self.data_path = data_path
        self.prefix = prefix
        self.multi = multi
        self.covar_level = covar_level
        self.covariate = covariate
        self.n_genotypes = n_genotypes
        self.num_workers = num_workers

        self.path_train_data_raw, self.path_test_data_raw = [
            self.data_path + self.prefix + train_test_ + self.covar_level + ".csv"
            for train_test_ in ("_train_", "_test_")]

        self.path_train_data, self.path_test_data = [
            self.data_path + self.prefix + train_test_ + self.covar_level + "_" + self.covariate + (
                "_multi" if self.multi else "_single") + ".csv"
            for train_test_ in ("_train_", "_test_")]

        self.batch_size = 1024

    def prepare_data(self):
        if Path(self.path_train_data).exists() and Path(self.path_test_data).exists():
            return None
        else:
            print("Preparing datasets")

        # Train data
        df_train_data = pd.read_csv(self.path_train_data_raw)
        # Test data
        df_test_data = pd.read_csv(self.path_test_data_raw)

        if self.multi:
            # Train data
            # Round temp to one decimal to allow grouping
            df_train_data[self.covariate] = np.round(df_train_data[self.covariate], 1)
            # Multi-trait data: Expand to all genotypes
            m_measurements = np.repeat(np.array(df_train_data['GR'])[:, np.newaxis],
                                       len(df_train_data['genotype.id'].unique()), axis=1)
            list_genotypes = df_train_data['genotype.id'].unique().tolist()
            m_genotypes = np.repeat(df_train_data['genotype.id'].unique()[np.newaxis, :], m_measurements.shape[0],
                                    axis=0)

            def is_genotype(a):
                return (a == df_train_data['genotype.id'])

            # NA cells where the value does not correspond to the genotype
            m_is_genotype = np.apply_along_axis(is_genotype, 0, m_genotypes)
            m_masked_measurements = m_measurements * m_is_genotype
            m_masked_measurements[m_masked_measurements == 0] = 'nan'
            # Add temperatures readings and sort by them
            m_masked_measurements = np.append(m_masked_measurements,
                                              np.array(df_train_data[self.covariate])[:, np.newaxis], axis=1)
            m_masked_measurements = m_masked_measurements[m_masked_measurements[:, self.n_genotypes].argsort()]
            list_genotypes.append(self.covariate)
            df_masked_measurements = pd.DataFrame(m_masked_measurements, columns=list_genotypes)
            # Compress per temperatur reading, remove NA values
            list_df_groups = []
            temp_groups = df_masked_measurements.groupby(self.covariate)
            for temp, df_group in temp_groups:
                df_group.drop(columns=self.covariate, inplace=True)
                df_group_compressed = pd.concat([s.dropna().reset_index(drop=True) for i, s in df_group.items()],
                                                axis=1)
                # Fill NA with mean
                df_group_compressed.fillna(df_group_compressed.mean(), inplace=True)
                df_group_compressed.fillna(np.nanmean(df_group_compressed), inplace=True)
                # Add temp column
                df_group_compressed[self.covariate] = temp
                list_df_groups.append(df_group_compressed)
            df_compressed_measurements = pd.concat(list_df_groups)
            df_compressed_measurements.to_csv(self.path_train_data, index=False)

            # Test data
            # Round temp to one decimal to allow grouping
            df_test_data[self.covariate] = np.round(df_test_data[self.covariate], 1)
            # Multi-trait data: Expand to all genotypes
            m_measurements = np.repeat(np.array(df_test_data['GR'])[:, np.newaxis],
                                       len(df_test_data['genotype.id'].unique()), axis=1)
            list_genotypes = df_test_data['genotype.id'].unique().tolist()
            m_genotypes = np.repeat(df_test_data['genotype.id'].unique()[np.newaxis, :], m_measurements.shape[0],
                                    axis=0)

            def is_genotype(a):
                return (a == df_test_data['genotype.id'])

            # NA cells where the value does not correspond to the genotype
            m_is_genotype = np.apply_along_axis(is_genotype, 0, m_genotypes)
            m_masked_measurements = m_measurements * m_is_genotype
            m_masked_measurements[m_masked_measurements == 0] = 'nan'
            # Add temperatures readings and sort by them
            m_masked_measurements = np.append(m_masked_measurements,
                                              np.array(df_test_data[self.covariate])[:, np.newaxis], axis=1)
            m_masked_measurements = m_masked_measurements[m_masked_measurements[:, self.n_genotypes].argsort()]
            list_genotypes.append(self.covariate)
            df_masked_measurements = pd.DataFrame(m_masked_measurements, columns=list_genotypes)
            df_masked_measurements.to_csv(self.path_test_data, index=False)

        else:
            df_train_data.to_csv(self.path_train_data, index=False)
            df_test_data.to_csv(self.path_test_data, index=False)

    def setup(self, stage=None):

        df_train_data = pd.read_csv(self.path_train_data)
        df_test_data = pd.read_csv(self.path_test_data)

        if self.multi:
            inputs_train = torch.tensor(np.array(np.array(df_train_data))[:, self.n_genotypes], dtype=torch.float)
            outputs_train = torch.tensor(np.array(np.array(df_train_data))[:, 0:self.n_genotypes], dtype=torch.float)

            inputs_test = torch.tensor(np.array(np.array(df_test_data))[:, self.n_genotypes], dtype=torch.float)
            outputs_test = torch.tensor(np.array(np.array(df_test_data))[:, 0:self.n_genotypes], dtype=torch.float)

            self.genotype_ids = df_train_data.columns


        else:
            inputs_train = torch.tensor([a for a in df_train_data[self.covariate]], dtype=torch.float)
            outputs_train = torch.tensor([a for a in df_train_data.GR], dtype=torch.float)

            inputs_test = torch.tensor([a for a in df_test_data[self.covariate]], dtype=torch.float)
            outputs_test = torch.tensor([a for a in df_test_data.GR], dtype=torch.float)

        data_train_all = TensorDataset(inputs_train, outputs_train)
        data_test_all = TensorDataset(inputs_test, outputs_test)

        self.train, self.val = random_split(
            data_train_all, [0.9, 0.1], generator=torch.Generator().manual_seed(1))

        self.test = data_test_all

    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.batch_size, num_workers=self.num_workers, persistent_workers=True)

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.batch_size, num_workers=self.num_workers, persistent_workers=True)

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.batch_size, num_workers=self.num_workers)


class BasicLLTRegressor(pl.LightningModule):

    def __init__(
            self):
        super().__init__()

        self.valid_R2_score = R2Score()
        self.test_R2_score = R2Score()

        self.learning_rate = 0.1

        # 1-10-1 network
        # First and second hidden layer: 1-8 and 8-8
        self.hid1 = torch.nn.Linear(1, 5)
        self.hid2 = torch.nn.Linear(5, 5)
        # Output layer: 8-1
        self.ALLout = torch.nn.Linear(5, 1)

        # Initialize
        torch.nn.init.xavier_uniform_(self.hid1.weight)
        torch.nn.init.zeros_(self.hid1.bias)
        torch.nn.init.xavier_uniform_(self.hid2.weight)
        torch.nn.init.zeros_(self.hid2.bias)
        torch.nn.init.xavier_uniform_(self.ALLout.weight)
        torch.nn.init.zeros_(self.ALLout.bias)

    def forward(self, input):
        output = torch.sigmoid(self.hid1(   (input.reshape(-1, 1)- 10) / 30  ))
        output = torch.sigmoid(self.hid2(output))
        output = self.ALLout(output).reshape(-1)
        return output

    def configure_optimizers(self):
        #optimizer = SGD(self.parameters(), lr=self.learning_rate)
        #scheduler = ExponentialLR(optimizer, gamma=0.9995)
        optimizer = Adam(self.parameters(), lr=self.learning_rate)
        scheduler = ExponentialLR(optimizer, gamma=0.996)

        return [optimizer], [scheduler]

        return

    def training_step(self, batch, batch_idx):

        input_i, target_i = batch
        pred_i = self.forward(input_i)

        pred_i = pred_i.reshape(-1)
        target_i = target_i.reshape(-1)

        loss = F.mse_loss(pred_i, target_i)

        self.log("train_mse", loss)

        return loss

    def validation_step(self, batch, batch_idx):

        input_i, target_i = batch
        pred_i = self.forward(input_i)

        pred_i = pred_i.reshape(-1)
        target_i = target_i.reshape(-1)

        loss = F.mse_loss(pred_i, target_i)

        self.log("valid_mse", loss, sync_dist=True)

        self.valid_R2_score(pred_i, target_i)

        self.log("valid_R2", self.valid_R2_score, sync_dist=True)

        return loss

    def on_train_epoch_end(self, *args, **kwargs):
        if self.current_epoch % 20 == 0:
            tensorboard = self.logger.experiment
            tensorboard.add_figure("dose-response-curve", self.generate_dose_response_curves(), global_step=self.current_epoch)

    def test_step(self, batch, batch_idx):

        input_i, target_i = batch
        pred_i = self.forward(input_i)

        target_i = target_i.reshape(-1)
        pred_i = pred_i.reshape(-1)

        pred_i = pred_i[~torch.isnan(target_i)]
        target_i = target_i[~torch.isnan(target_i)]

        self.test_R2_score(pred_i, target_i)

        self.log("test_R2", self.test_R2_score, sync_dist=True)

        return self.test_R2_score

    def generate_dose_response_curves(self, plot_path=None, start=-1, stop=20, steps=100):

        input = np.linspace(start, stop, steps)
        input_doses = torch.tensor((input), dtype=torch.float)

        batch = self.transfer_batch_to_device(input_doses, self.device, dataloader_idx=0)
        output_values = self.predict_step(batch, 0)

        df_data = pd.DataFrame({"temp": input_doses.detach(),
                                "growth_rate": output_values.cpu().detach().reshape(-1),
                                "gen": "general"})

        fig = plt.figure(figsize=(8, 6))
        sns.set(style="whitegrid")
        sns.lineplot(df_data,
                     x="temp",
                     y="growth_rate",
                     color='green',
                     linewidth=2.5)
        plt.xlabel('Temperature (°C)')
        plt.ylabel('Growth rate (mm/h)')
        if not plot_path:
            plt.ylim(bottom=-0.05, top=1.6)

        plt.tight_layout()

        if plot_path:
            plt.savefig(plot_path)
            plt.close()
            return None
        else:
            return fig


class GenotypeLLTRegressor(BasicLLTRegressor):

    def __init__(
            self, n_genotypes=12, l1_lambda=0.001):
        super().__init__()

        self.n_genotypes = n_genotypes
        self.learning_rate = 0.05
        self.l1_lambda = l1_lambda

        # Second hidden layer group: 1-8-8-ngen for genotype specificity
        #self.hid3 = torch.nn.Linear(1, 5 * self.n_genotypes)
        #self.hid4 = torch.nn.Linear(5 * self.n_genotypes, 5 * self.n_genotypes)
        # Genotype specific thermal time output layer: 5-1
        self.GENout = torch.nn.Linear(1, self.n_genotypes)

        # Initialize
        #torch.nn.init.xavier_uniform_(self.hid3.weight)
        #torch.nn.init.zeros_(self.hid3.bias)
        #torch.nn.init.xavier_uniform_(self.hid4.weight)
        #torch.nn.init.zeros_(self.hid4.bias)

        torch.nn.init.constant_(self.GENout.weight, 1)
        torch.nn.init.zeros_(self.GENout.bias)

    def forward(self, input):
        global_pipe = torch.sigmoid(self.hid1(   (input.reshape(-1, 1)- 10) / 30   ))
        global_pipe = torch.sigmoid(self.hid2(global_pipe))
        global_pipe = self.ALLout(global_pipe)
        global_pipe = torch.clamp(global_pipe, 0)

        #genotype_pipe = torch.sigmoid(self.hid3(   (input.reshape(-1, 1) - 10) / 30   ))
        #genotype_pipe = torch.sigmoid(self.hid4(genotype_pipe))
        genotype_pipe = self.GENout(global_pipe)

        output = genotype_pipe

        return output

    def forward_for_predict(self, input):
        global_pipe = torch.sigmoid(self.hid1(   (input.reshape(-1, 1)- 10) / 30   ))
        global_pipe = torch.sigmoid(self.hid2(global_pipe))
        global_pipe = self.ALLout(global_pipe)
        global_pipe = torch.clamp(global_pipe, 0)

        #genotype_pipe = torch.sigmoid(self.hid3(   (input.reshape(-1, 1) - 10) / 30   ))
        #genotype_pipe = torch.sigmoid(self.hid4(genotype_pipe))
        genotype_pipe = self.GENout(global_pipe)

        output = genotype_pipe
        output = torch.cat([output, global_pipe], 1)

        return output

    def training_step(self, batch, batch_idx):

        input_i, target_i = batch
        pred_i = self.forward(input_i)

        pred_i = pred_i.reshape(-1)
        target_i = target_i.reshape(-1)

        loss = F.mse_loss(pred_i, target_i)

        # Regularisation: Give more power to the NN before the final linear regression (GENout) by forcing GENout in the direction y = 1 * x + 0
        l1_weights = sum((p - 1).abs().sum()
                      for p in self.GENout.weight)

        l1_bias = sum(p.abs().sum()
                      for p in self.GENout.bias)

        l1_norm = l1_weights + l1_bias

        loss += self.l1_lambda * l1_norm

        self.log("train_loss", loss)

        return loss

    def predict_step(self, batch, batch_idx):
        return self.forward_for_predict(batch)

    def generate_dose_response_df(self, start=-1, stop=20, steps=100):

        input = np.linspace(start, stop, steps)
        input_doses = torch.tensor((input), dtype=torch.float)

        batch = self.transfer_batch_to_device(input_doses, self.device, dataloader_idx=0)
        output_values = self.predict_step(batch, 0)
        output_values = output_values.cpu()

        df_data = pd.DataFrame({"x": np.repeat(input_doses, output_values.detach().shape[1]),
                                "y": output_values.detach().reshape(-1),
                                "gen": [("g" + str(round(a)) if a < output_values.detach().shape[1] else "general")
                                        for a in np.tile(
                                        np.linspace(1, output_values.detach().shape[1],
                                                    output_values.detach().shape[1]),
                                        output_values.detach().shape[0])]})

        return df_data

    def generate_dose_response_curves(self, plot_path=None, start=-1, stop=20, steps=100):

        df_data = self.generate_dose_response_df(start=-1, stop=20, steps=100)
        fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(16, 6))
        sns.set(style="whitegrid")

        sns.lineplot(df_data[df_data.gen != "general"], x="x",
                     y="y",
                     units='gen',
                     estimator=None,
                     color="blue",
                     linewidth=1.5, ax=ax[0])
        sns.lineplot(df_data[df_data.gen == "general"], x="x",
                     y="y",
                     color="orange",
                     linewidth=2.5,
                     ax=ax[0])
        ax[0].set_ylabel('Growth rate (mm(h)')
        ax[0].set_xlabel('Temperature (°C)')
        ax[0].set_title("(a)")
        if not plot_path:
            ax[0].set_ylim(bottom=-0.05, top=1.6)

        df_data_g = pd.merge(df_data[df_data.gen != "general"], df_data[df_data.gen == "general"].drop("gen", axis=1),
                             on="x")
        df_data_g['dy'] = df_data_g["y_x"] - df_data_g["y_y"]

        sns.set(style="whitegrid")
        sns.lineplot(df_data_g, x="x",
                     y="dy",
                     units='gen',
                     estimator=None,
                     dashes=False,
                     linewidth=1.5,
                     color="blue",
                     ax=ax[1])
        ax[1].set_ylabel('Growth rate (mm/h)')
        ax[1].set_xlabel('Temperature (°C)')
        ax[1].set_title("(b)")
        if not plot_path:
            ax[1].set_ylim(bottom=-0.3, top=0.3)

        plt.tight_layout()

        if plot_path:
            plt.savefig(plot_path + ".pdf")
            plt.close()
            return None
        else:
            return fig




########################################################################
# Main
if __name__ == '__main__':

    freeze_support()

    lightning_logs = "/home/luroth/nashome/lightning"
    #lightning_logs = "lightning"
    fast_dev_run = False
    accelerator = "gpu"
    devices = [0, 1, 2]
    num_workers = 10

    #

    # lightning_logs = "lightning_logs"
    # fast_dev_run = True
    # accelerator = "cpu"
    # devices = 1

    model_confs = (
        ("LLT_wheat", "ref", "temp_air_10cm", 12),
        ("LLT_wheat", "ref", "temp_soil_5cm", 12),
        ("LLT_wheat", "plot", "temp_air_10cm", 12),
        ("LLT_wheat", "plot", "temp_soil_5cm", 12),
        ("TRACK_soy", "ref", "temp_air_10cm", 3),
        ("TRACK_soy", "ref", "temp_soil_5cm", 3)
    )

    for model_conf in model_confs:
        print("MODEL FOR " + " ".join([str(a) for a in model_conf]))
        prefix, covar_level, covariate, n_genotypes = model_conf

        # Models
        # Single
        tb_logger = pl_loggers.TensorBoardLogger(save_dir=lightning_logs, name="_".join((prefix, covar_level, covariate, "general")))
        csv_logger = pl_loggers.CSVLogger(save_dir=lightning_logs, name="_".join((prefix, covar_level, covariate, "general")))

        model_single = BasicLLTRegressor()
        model_multi = GenotypeLLTRegressor(n_genotypes)
        model_multi_refine = GenotypeLLTRegressor(n_genotypes)
        #if prefix == "LLT_wheat":
        #    model_multi.load_state_dict(torch.load("Python/init_multi.pt"))

        # Data
        dataloader_single = LLTDataModule(data_path="Data_NN_train/", prefix=prefix, multi=False,
                                          covar_level=covar_level, covariate=covariate, n_genotypes=n_genotypes, num_workers=num_workers)
        dataloader_multi = LLTDataModule(data_path="Data_NN_train/", prefix=prefix, multi=True, covar_level=covar_level,
                                         covariate=covariate, n_genotypes=n_genotypes, num_workers=num_workers)

        # Train
        lr_monitor = LearningRateMonitor(logging_interval='epoch')
        early_stopping = EarlyStopping('valid_mse', min_delta=0.0001, patience=40)

        n_epochs = 10000
        n_epochs = 1500

        trainer = pl.Trainer(max_epochs=n_epochs, accelerator=accelerator,
                             devices=devices,
                             strategy=DDPStrategy(find_unused_parameters=False),
                             fast_dev_run=fast_dev_run,
                             default_root_dir=lightning_logs,
                             logger=[tb_logger, csv_logger],
                             benchmark=True,
                             precision=16,
                             check_val_every_n_epoch=1,
                             callbacks=[lr_monitor, early_stopping]
                             )

        dataloader_single.batch_size = 8192 + 1024

        # Train basic model
        model_single.learning_rate = 0.05

        trainer.fit(model_single, dataloader_single)
        trainer.test(model_single, dataloader_single)

        model_single.generate_dose_response_curves(
            lightning_logs + '/graphs/NN_general_' + prefix + "_" + covar_level + "_" + covariate + '.pdf')
        torch.save(model_single.state_dict(),
                   lightning_logs + "/trained_models/model_simple_state_" + prefix + "_" + covar_level + "_" + covariate + ".pt")

        # Train genotype specific model
        tb_logger = pl_loggers.TensorBoardLogger(save_dir=lightning_logs, name="_".join((prefix, covar_level, covariate, "genotype")))
        csv_logger = pl_loggers.CSVLogger(save_dir=lightning_logs, name="_".join((prefix, covar_level, covariate, "genotype")))

        n_epochs = 10000
        n_epochs = 800
        trainer = pl.Trainer(max_epochs=n_epochs, accelerator=accelerator,
                             devices=devices,
                             strategy=DDPStrategy(find_unused_parameters=False),
                             fast_dev_run=fast_dev_run,
                             default_root_dir=lightning_logs,
                             logger=[tb_logger, csv_logger],
                             benchmark=True,
                             precision=16,
                             check_val_every_n_epoch=1,
                             callbacks=[lr_monitor, early_stopping]
                             )
        dataloader_multi.batch_size = 2000

        # Extract estimated parameters from simple model
        state_model_single = model_single.state_dict()
        model_multi.load_state_dict(state_model_single, strict=False)
        # Freeze layers corresponding to simpler model
        ct = 0
        for child in model_multi.children():
            ct += 1
            if ct <= 5:
                for param in child.parameters():
                    param.requires_grad = False
        model_multi.learning_rate = 0.05
        trainer.fit(model_multi, dataloader_multi)
        trainer.test(model_multi, dataloader_multi)

        model_multi.generate_dose_response_curves(
            lightning_logs + '/graphs/NN_genotypes_' + prefix + "_" + covar_level + "_" + covariate + '.pdf')
        torch.save(model_multi.state_dict(),
                   lightning_logs + "/trained_models/model_multi_state_" + "_" + prefix + "_" + covar_level + "_" + covariate + ".pt")

        # Retrain with unfreezed layers
        tb_logger = pl_loggers.TensorBoardLogger(save_dir=lightning_logs, name="_".join((prefix, covar_level, covariate, "genotype_refine")))
        csv_logger = pl_loggers.CSVLogger(save_dir=lightning_logs, name="_".join((prefix, covar_level, covariate, "genotype_refine")))

        n_epochs = 10000
        n_epochs = 800
        trainer = pl.Trainer(max_epochs=n_epochs, accelerator=accelerator,
                             devices=devices,
                             strategy=DDPStrategy(find_unused_parameters=False),
                             fast_dev_run=fast_dev_run,
                             default_root_dir=lightning_logs,
                             logger=[tb_logger, csv_logger],
                             benchmark=True,
                             precision=16,
                             check_val_every_n_epoch=1,
                             callbacks=[lr_monitor]
                            )

        # Extract estimated parameters from multi model
        state_model_multi = model_multi.state_dict()
        model_multi_refine.load_state_dict(state_model_multi, strict=False)
        for child in model_multi_refine.children():
            for param in child.parameters():
                param.requires_grad = True
        model_multi_refine.learning_rate = 0.05
        trainer.fit(model_multi_refine, dataloader_multi)
        trainer.test(model_multi_refine, dataloader_multi)

        model_multi_refine.generate_dose_response_curves(
            lightning_logs + '/graphs/NN_genotypes_' + prefix + "_" + covar_level + "_" + covariate + '_refine.pdf')
        torch.save(model_multi_refine.state_dict(),
                   lightning_logs + "/trained_models/model_multifine_state_" + prefix + "_" + covar_level + "_" + covariate + "_refine.pt")

        df_dose_response = model_multi_refine.generate_dose_response_df(start=-10, stop=40, steps=500)
        df_dose_response.columns = ["temp", "GR", "genotype.id"]
        replace_dict = {"g" + str(seq + 1) : genotype_id for (seq, genotype_id) in enumerate(dataloader_multi.genotype_ids[0: n_genotypes])}
        df_dose_response.replace(replace_dict, inplace=True)

        df_dose_response.to_csv(lightning_logs + '/graphs/NN_genotypes_' + prefix + "_" + covar_level + "_" + covariate + '_lookup.csv')