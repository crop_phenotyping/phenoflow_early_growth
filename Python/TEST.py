import torch # torch will allow us to create tensors.
import torch.nn as nn # torch.nn allows us to create a neural network.
import torch.nn.functional as F # nn.functional give us access to the activation and loss functions.
from torch.optim import SGD # optim contains many optimizers. Here, we're using SGD, stochastic gradient descent.

import pytorch_lightning as pl
from torch.utils.data import TensorDataset, DataLoader # these are needed for the training data

import matplotlib.pyplot as plt ## matplotlib allows us to draw graphs.
import seaborn as sns ## seaborn makes it easier to draw nice-looking graphs.

from lightning_lite.utilities.seed import seed_everything # this is added because people on different computers were
seed_everything(seed=42)

import numpy as np
import pandas as pd

## Create a neural network class that we can train by creating a class that inherits from LightningModule
##
## NOTE: This new class, BasicLightningTrain, contains two new methods for training:
##
## training_step() - This method takes care of 4 things:
##      a) calculates the loss for an epoch
##      b) resets the gradients
##      c) backpropagation
##      d) updates the parameters
## configure_optimizers() - defines the method we will use to optimize the model
class BasicLightningTrain(pl.LightningModule):

    def __init__(
            self):  # __init__() is the class constructor function, and we use it to initialize the weights and biases.

        ## NOTE: The code for __init__ () is the same as before except we now have a learning rate parameter (for
        ##       gradient descent) and we modified final_bias in two ways:
        ##           1) we set the value of the tensor to 0, and
        ##           2) we set "requires_grad=True".

        super().__init__()  # initialize an instance of the parent class, LightningModule.

        self.w00 = nn.Parameter(torch.tensor(1.7), requires_grad=True)
        self.b00 = nn.Parameter(torch.tensor(-0.85), requires_grad=True)
        self.w01 = nn.Parameter(torch.tensor(-40.8), requires_grad=True)

        self.w10 = nn.Parameter(torch.tensor(12.6), requires_grad=True)
        self.b10 = nn.Parameter(torch.tensor(0.0), requires_grad=True)
        self.w11 = nn.Parameter(torch.tensor(2.7), requires_grad=True)

        ## We want to modify final_bias to demonstrate how to optimize it with backpropagation.
        ## NOTE: The optimal value for final_bias is -16...
        #         self.final_bias = nn.Parameter(torch.tensor(-16.), requires_grad=False)
        ## ...so we set it to 0 and tell Pytorch that it now needs to calculate the gradient for this parameter.
        self.final_bias = nn.Parameter(torch.tensor(0.0), requires_grad=True)

        self.learning_rate = 0.00214  ## this is for gradient descent. NOTE: we will improve this value later, so, technically
        ## this is just a placeholder until then. In other words, we could put any value here
        ## because later we will replace it with the improved value.

        # 1-10-1 network
        # First hidden layer: 1-10
        self.hid1 = torch.nn.Linear(1, 4)
        self.hid2 = torch.nn.Linear(4, 4)
        # Output layer: 10-1
        self.oupt = torch.nn.Linear(4, 1)

        # Initialize
        torch.nn.init.xavier_uniform_(self.hid1.weight)
        torch.nn.init.zeros_(self.hid1.bias)
        torch.nn.init.xavier_uniform_(self.oupt.weight)
        torch.nn.init.zeros_(self.oupt.bias)

    def forward(self, input):
        ## forward() is the exact same as before

        output = torch.relu(self.hid1(input.reshape(-1,1)))
        output = torch.relu(self.hid2(output))
        output = self.oupt(output).reshape(-1)
        return output

    def configure_optimizers(self):  # this configures the optimizer we want to use for backpropagation.
        return SGD(self.parameters(), lr=self.learning_rate)  # NOTE: We set the learning rate (lr) to our new variable
        # self.learning_rate

    def training_step(self, batch, batch_idx):  # take a step during gradient descent.

        ## NOTE: When training_step() is called it calculates the loss with the code below...
        input_i, label_i = batch  # collect input
        output_i = self.forward(input_i)  # run input through the neural network
        loss = (output_i - label_i) ** 2  ## loss = squared residual

        ##...before calling (internally and behind the scenes)...
        ## optimizer.zero_grad() # to clear gradients
        ## loss.backward() # to do the backpropagation
        ## optimizer.step() # to update the parameters
        return loss

## create the neural network.
model = BasicLightningTrain()

input_doses = torch.linspace(start=0, end=1, steps=50)

## now run the different doses through the neural network.
output_values = model(input_doses)

## Now draw a graph that shows the effectiveness for each dose.
##
## set the style for seaborn so that the graph looks cool.
sns.set(style="whitegrid")

## create the graph (you might not see it at this point, but you will after we save it as a PDF).
sns.lineplot(x=input_doses,
             y=output_values.detach(), ## NOTE: because final_bias has a gradident, we call detach()
                                       ## to return a new tensor that only has the value and not the gradient.
             color='green',
             linewidth=2.5)

## now label the y- and x-axes.
plt.ylabel('Effectiveness')
plt.xlabel('Dose')
plt.show()

## lastly, save the graph as a PDF.
#plt.savefig('BasicLightningTrain.pdf')

## create the training data for the neural network.
# inputs = torch.tensor([0., 0.5, 1.])
# labels = torch.tensor([0., 1., 0.])
## NOTE: Because we have so little data, and let's be honest, it's an unrealistically small
## amount of data, the learning rate algorithm, lr_find(), that we use in the next section has trouble.
## So, the point here is to show how to use lr_find() when you have a reasonable amount of data,

# Load data
df_data = pd.read_csv("Data_wheat_LLT_extract/data_plot.csv")
inputs_raw = torch.tensor([a for a in df_data.temp_air_10cm], dtype=torch.float)
inputs_min, inputs_max = torch.min(inputs_raw), torch.max(inputs_raw)
inputs_norm  = (inputs_raw - inputs_min) / (inputs_max - inputs_min)
torch.min(inputs_norm), torch.max(inputs_norm)

outputs_raw = torch.tensor([a for a in df_data.GR], dtype=torch.float)
outputs_min, outputs_max = torch.min(outputs_raw), torch.max(outputs_raw)
outputs_norm  = (outputs_raw - outputs_min) / (outputs_max - outputs_min)
torch.min(outputs_norm), torch.max(outputs_norm)

dataset = TensorDataset(inputs_norm, outputs_norm)
dataloader = DataLoader(dataset)

model = BasicLightningTrain() # First, make model from the class

## Now create a Trainer - we can use the trainer to...
##  1) Find the optimal learning rate
##  2) Train (optimize) the weights and biases in the model
## By default, the trainer will run on your system's CPU
## However, if we wantafter activation always sum?ed to automatically take advantage of any available GPUs,
## we would set accelerator="auto" to automatically use available GPUs
## and we would set devices="auto" to automatically select as many GPUs as we have.
#
trainer = pl.Trainer(max_epochs=20, accelerator="cpu")

## Now let's find the optimal learning rate
lr_find_results = trainer.tuner.lr_find(model,
                                        train_dataloaders=dataloader, # the training data
                                        min_lr=0.001, # minimum learning rate
                                        max_lr=1.0,   # maximum learning rate
                                        early_stop_threshold=None) # setting this to "None" tests all 100 candidate rates
new_lr = lr_find_results.suggestion() ## suggestion() returns the best guess for the optimal learning rate

## now print out the learning rate
#print(f"lr_find() suggests {new_lr:.5f} for the learning rate.")

model.learning_rate = new_lr

## Now that we have an improved learning rate, we can train the model (optimize final_bias)

trainer.fit(model, train_dataloaders=dataloader)

## now run the different doses through the neural network.
output_values = model(input_doses)

## Now draw a graph that shows the effectiveness for each dose.
##
## set the style for seaborn so that the graph looks cool.
sns.set(style="whitegrid")

## create the graph (you might not see it at this point, but you will after we save it as a PDF).
sns.lineplot(x=input_doses,
             y=output_values.detach(), ## NOTE: because final_bias has a gradident, we call detach()
                                       ## to return a new tensor that only has the value and not the gradient.
             color='green',
             linewidth=2.5)

## now label the y- and x-axes.
plt.ylabel('Effectiveness')
plt.xlabel('Dose')
plt.show()